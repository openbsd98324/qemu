# qemu

## Raspberry PI 

http://downloads.raspberrypi.org/raspbian/images/raspbian-2017-04-10/2017-04-10-raspbian-jessie.zip


Now you can emulate it on Qemu by using the following command:

$ qemu-system-arm -kernel ~/qemu_vms/<your-kernel-qemu> -cpu arm1176 -m 256 -M versatilepb -serial stdio -append "root=/dev/sda2 rootfstype=ext4 rw" -hda ~/qemu_vms/<your-jessie-image.img> -redir tcp:5022::22 -no-reboot

If you see GUI of the Raspbian OS, you need to get into the terminal. Use Win key to get the menu, then navigate with arrow keys until you find Terminal application as shown below.

## Installation Portable

## Installation portable Win (need admin root!)

https://gitlab.com/openbsd98324/qemu-portable/-/raw/main/archive/qemu-w64-setup-20220418.exe


## Example

qemu-system-i386 -M pc -bios ~/qemu/bios.bin


## Convert Image Raw => vmdk

````
qemu-img convert imagefile.dd -O vmdk vmdkname.vmdk
````

## Convert Image vmdk => Raw

Run the following command to convert a vmdk image file to a raw image file.

````
$ qemu-img convert -f vmdk -O raw image.vmdk image.img

````


## Convert Image Raw => qcow2

````
qemu-img convert imagefile-raw-src.img  -O qcow2 image-target.qcow2
````





## NetBSD using no graphics


Tested: You can add: -curses    

Alternatively : -nographic may work as well. 





## NetBSD over SSH (nographic): SSH + TMUX + Qemu (no graphic)

1.) ssh to distant machine

2.) qemu -curses dual-boot.qcow2

3.) boot with grub to netbsd



![](medias/1640362476-1-netbsd-over-qemu-curses-ssh.png)

![](medias/1640362476-1-netbsd-over-qemu-curses-ssh-running.png)

